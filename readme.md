# The Pivo Core service
This service acts as the entrypoint to the Pivo backend, while also holding the majority of the business logic.
Built with Elixir, GrahpQL and Phoenix. See it in action [here](https://api.pivo.mihaimiuta.eu/graphql).

## Running
* Clone the repository
* Run `docker compose up`

## Overall platform architecture
The Pivo backend is currently composed of the following services:
* Core - The entrypoint service
* Predictor - A Scala REST Web API (built with http4s) which is in charge of predicting beer categories
* Tensorflow - A docker container with Tensorflow, opencv and numpy installed
* Database - A Redis database server that saves as the application's data service
* File Storage - A Minio server which is meant to store customer recorded new categories

## GraphQL Schema
Here's the current (hopefully up to date) GraphQL Schema of the Core Service:
```graphql
"""
categories - fetches the categories currently present in the system
"""
query {
	categories {
		createdAt
		name
		previewImage
	}
}

"""
scan - sends a base64 representation of an image in order to predict its category
"""
query scan($imageBase64: String!) {
	scan(imageBase64: $imageBase64) {
		name
		previewImage
		createdAt
	}
}

"""
isAdmin - receives an email as a parameter and checks whether the given email belongs to a platform admin
"""
query isAdmin($email: String!) {
	isAdmin(email: $email) 
}

```