defmodule CoreWeb.Mutation do
  defmacro __using__(_opts) do
    quote do
      mutation do
        field :login, type: :login do
          @desc "Generates a JWT for the provided credentials"
          arg(:username, non_null(:string))
          arg(:password, non_null(:string))

          resolve(&CoreWeb.Resolvers.Auth.login/3)
        end

        field :register, type: :login do
          @desc "Creates a new user with the provided credentials and generates a JWT for them"
          arg(:username, non_null(:string))
          arg(:password, non_null(:string))

          resolve(&CoreWeb.Resolvers.Auth.register/3)
        end

        field :logout, type: :boolean do
          @desc "Logs out the user"

          resolve(&CoreWeb.Resolvers.Auth.logout/3)
        end

        field :get_access_token, type: :login do
          @desc "Creates an access token for the given refresh token"
          arg(:refresh_token, non_null(:string))

          resolve(&CoreWeb.Resolvers.Auth.get_access_token/3)
        end

        field :add_review, type: :review do
          @desc "Creates a new review"
          arg(:name, non_null(:string))
          arg(:score, non_null(:integer))

          resolve(&CoreWeb.Resolvers.Reviews.create/3)
        end

        field :update_review, type: :review do
          @desc "Updates a review"
          arg(:name, non_null(:string))
          arg(:score, non_null(:integer))

          resolve(&CoreWeb.Resolvers.Reviews.update/3)
        end

        field :delete_review, type: :review do
          @desc "Deletes a review"
          arg(:name, non_null(:string))

          resolve(&CoreWeb.Resolvers.Reviews.delete/3)
        end

        field :add_comment, type: :comment do
          @desc "Creates a new comment"
          arg(:name, non_null(:string))
          arg(:text, non_null(:string))

          resolve(&CoreWeb.Resolvers.Comments.create/3)
        end

        field :update_comment, type: :comment do
          @desc "Updates a comment"
          arg(:name, non_null(:string))
          arg(:created_at, non_null(:string))
          arg(:text, non_null(:string))

          resolve(&CoreWeb.Resolvers.Comments.update/3)
        end

        field :delete_comment, type: :comment do
          @desc "Deletes a comment"
          arg(:name, non_null(:string))
          arg(:created_at, non_null(:string))

          resolve(&CoreWeb.Resolvers.Comments.delete/3)
        end

        field :approve_pending, type: :pending do
          @desc "Approves a potential category"
          arg(:name, non_null(:string))

          resolve(&CoreWeb.Resolvers.Potential.approve/3)
        end

        field :reject_pending, type: :pending do
          @desc "Reject a potential category"
          arg(:name, non_null(:string))

          resolve(&CoreWeb.Resolvers.Potential.reject/3)
        end

        field :add_pending, type: :boolean do
          @desc "Add a potential category"
          arg(:chunk, non_null(:string))

          resolve(&CoreWeb.Resolvers.Potential.add/3)
        end
      end
    end
  end
end
