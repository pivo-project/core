defmodule CoreWeb.Schema do
  use Absinthe.Schema

  import_types(CoreWeb.ContentTypes)

  use CoreWeb.Query
  use CoreWeb.Mutation
end
