defmodule CoreWeb.Resolvers.Scan do
  import CoreWeb.Resolvers.Common

  def execute(_, args, %{context: context}) do
    image_base64 = Map.get(args, :image_base64)
    current_user = Map.get(context, :current_user)

    {:ok, Core.Scan.execute(current_user, image_base64)}
  end

  def get(_, _, %{context: context}) do
    authorised(context, fn current_user ->
      Core.Scan.Get.execute(current_user)
    end)
  end
end
