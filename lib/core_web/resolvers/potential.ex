defmodule CoreWeb.Resolvers.Potential do
  def get(_, _, _),
    do:
      {:ok,
       [
         %{
           name: "MyHand",
           created_at: :os.system_time(:millisecond),
           video: "something"
         }
       ]}

  def approve(_, _, _),
    do:
      {:ok,
       %{
         name: "MyHand",
         created_at: :os.system_time(:millisecond),
         video: "something"
       }}

  def reject(_, _, _),
    do:
      {:ok,
       %{
         name: "MyHand",
         created_at: :os.system_time(:millisecond),
         video: "something"
       }}

  def add(_, _, _) do
    :timer.sleep(1000)

    {:ok, true}
  end
end
