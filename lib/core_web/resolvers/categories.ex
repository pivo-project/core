defmodule CoreWeb.Resolvers.Categories do
  defp cast(category), do: Map.from_struct(category)

  def get(_, _, _), do: {:ok, Core.Categories.get() |> Enum.map(&cast/1)}
end
