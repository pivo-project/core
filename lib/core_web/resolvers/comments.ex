defmodule CoreWeb.Resolvers.Comments do
  def get(_, _, _), do: {:ok, []}

  def create(_, _, _), do: {:ok, []}

  def update(_, _, _), do: {:ok, []}

  def delete(_, _, _), do: {:ok, []}
end
