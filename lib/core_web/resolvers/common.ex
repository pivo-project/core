defmodule CoreWeb.Resolvers.Common do
  alias Core.RefreshToken

  defp user_not_nil?(current_user), do: !is_nil(current_user)

  defp refresh_token_valid?(token) do
    case RefreshToken.verify_with_salt(token) do
      {:ok} -> true
      {:error} -> false
    end
  end

  defp refresh_token_exists?(token) do
    case RefreshToken.verify_saved(token) do
      {:ok} -> true
      {:error} -> false
    end
  end

  def authorised(context, on_okay) do
    with current_user <- Map.get(context, :current_user),
         true <- user_not_nil?(current_user) do
      on_okay.(current_user)
    else
      _ -> {:error, "unauthorised"}
    end
  end

  def authenticated(refresh_token, on_okay) do
    with true <- refresh_token_valid?(refresh_token),
         true <- refresh_token_exists?(refresh_token) do
      on_okay.()
    else
      _ -> {:error, "unauthenticated"}
    end
  end
end
