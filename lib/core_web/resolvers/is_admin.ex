defmodule CoreWeb.Resolvers.IsAdmin do
  def execute(_, args, _) do
    admins = Core.Admins.get()
    email = Map.get(args, :email)

    if (Enum.any?(admins, fn admin -> admin == email end)) do
      {:ok, true}
    else
      {:ok, false}
    end
  end
end
