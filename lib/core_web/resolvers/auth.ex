defmodule CoreWeb.Resolvers.Auth do
  alias Core.Users
  alias Core.Token
  alias Core.RefreshToken
  import CoreWeb.Resolvers.Common

  def login(_, %{username: username, password: password}, _) do
    case Users.get(username, password) do
      {:ok, user} ->
        access_token = Token.gen_for_user(user)
        refresh_token = RefreshToken.gen_for_user(user)
        {:ok} = Users.Token.save(refresh_token, username)

        {:ok, %{access_token: access_token, refresh_token: refresh_token}}

      {:error, message} ->
        {:error, message}
    end
  end

  def register(_, %{username: username, password: password}, _) do
    case Users.create(username, password) do
      {:ok, user} ->
        access_token = Token.gen_for_user(user)
        refresh_token = RefreshToken.gen_for_user(user)
        {:ok} = Users.Token.save(refresh_token, username)

        {:ok, %{access_token: access_token, refresh_token: refresh_token}}

      {:error, message} ->
        {:error, message}
    end
  end

  def logout(_, _, %{context: context}) do
    authorised(context, fn current_user ->
      Users.Token.delete(current_user)
    end)
  end

  def get_access_token(_, %{refresh_token: refresh_token}, _) do
    authenticated(refresh_token, fn ->
      with {:ok, username} <- RefreshToken.get_username(refresh_token),
           {:ok, user} <- Users.get(username) do
        access_token = Token.gen_for_user(user)

        {:ok, %{access_token: access_token, refresh_token: refresh_token}}
      else
        _ -> {:error, "failed to generate access token"}
      end
    end)
  end
end
