defmodule CoreWeb.Context do
  @behaviour Plug

  import Plug.Conn

  def init(opts), do: opts

  def call(conn, _) do
    context = build_context(conn)

    Absinthe.Plug.put_options(conn, context: context)
  end

  defp build_context(conn) do
    with ["Bearer " <> token] <- get_req_header(conn, "authorization"),
         {:ok, current_user} <- decode(token),
         {:ok} <- validate(token, current_user) do
      %{current_user: current_user}
    else
      _ -> %{}
    end
  end

  defp validate(token, current_user) do
    {:ok, user} = Core.Users.get(current_user)

    Core.Token.verify_for_user(token, user)
  end


  defp decode(token), do: {:ok, Core.Token.get_username(token)}
end
