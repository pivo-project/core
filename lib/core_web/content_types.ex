defmodule CoreWeb.ContentTypes do
  use Absinthe.Schema.Notation

  @desc "A category"
  object :category do
    field :name, :string
    field :created_at, :string
    field :preview_image, :string
    field :average_score, :float
  end

  @desc "Login result"
  object :login do
    field :access_token, :string
    field :refresh_token, :string
  end

  @desc "A scan log"
  object :scan_log do
    field :name, :string
    field :timestamp, :string
    field :preview_image, :string
  end

  @desc "A review"
  object :review do
    field :score, :integer
    field :created_at, :string
    field :updated_at, :string
    field :author, :string
  end

  @desc "A comment"
  object :comment do
    field :text, :string
    field :author, :string
    field :created_at, :string
    field :updated_at, :string
  end

  @desc "A potential category pending admin review"
  object :pending do
    field :name, :string
    field :created_at, :string
    field :video, :string
  end
end
