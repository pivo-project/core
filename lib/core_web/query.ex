defmodule CoreWeb.Query do
  defmacro __using__(_opts) do
    quote do
      query do
        @desc "Get categories"
        field :categories, list_of(:category) do
          resolve(&CoreWeb.Resolvers.Categories.get/3)
        end

        @desc "Scan a beer"
        field :scan, :category do
          arg(:image_base64, non_null(:string))

          resolve(&CoreWeb.Resolvers.Scan.execute/3)
        end

        @desc "Checks if a given user is an admin or not"
        field :is_admin, :boolean do
          arg(:email, non_null(:string))

          resolve(&CoreWeb.Resolvers.IsAdmin.execute/3)
        end

        @desc "Gets the scan history of a user"
        field :scan_history, list_of(:scan_log) do
          resolve(&CoreWeb.Resolvers.Scan.get/3)
        end

        @desc "Gets the comments for a given category"
        field :comments, list_of(:comment) do
          arg(:category, non_null(:string))

          resolve(&CoreWeb.Resolvers.Comments.get/3)
        end

        @desc "Gets the reviews for a given category"
        field :reviews, list_of(:review) do
          arg(:category, non_null(:string))

          resolve(&CoreWeb.Resolvers.Reviews.get/3)
        end

        @desc "Gets potential categories pending admin review"
        field :pending, list_of(:pending) do
          resolve(&CoreWeb.Resolvers.Potential.get/3)
        end
      end
    end
  end
end
