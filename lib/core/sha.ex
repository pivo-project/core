defmodule Core.Sha do
  def encrypt(message, key) do
    :crypto.mac(:hmac, :sha256, key, message)
    |> Base.encode16()
  end
end
