defmodule Core.Admins do
  def set(admins) do
    conn = Core.Redis.get_connection()

    {:ok, admins_json} = Poison.encode(admins)

    {:ok, "OK"} = Redix.command(conn, ["JSON.SET", "admins", ".", admins_json])

    :ok
  end

  def get() do
    conn = Core.Redis.get_connection()

    {:ok, admins_json} = Redix.command(conn, ["JSON.GET", "admins"])

    if is_nil(admins_json) do
      []
    else
      Poison.decode!(admins_json)
    end
  end
end
