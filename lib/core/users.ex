defmodule Core.Users do
  alias Core.Users.Queries

  def get(username) do
    conn = Core.Redis.get_connection()

    {:ok, [["user"], result, _]} =
      Redix.command(conn, ["GRAPH.QUERY", "Users", Queries.get(username)])

    users = Core.Redis.Query.parse_result(result)

    if Enum.empty?(users) do
      {:error, "User doesn't exist"}
    else
      [user] = users

      {:ok, user}
    end
  end

  def get(username, password) do
    case get(username) do
      {:error, message} ->
        {:error, message}

      {:ok, user} ->
        hashed_input = Core.Sha.encrypt(password, Map.get(user, "salt"))

        if hashed_input != Map.get(user, "password_hash") do
          {:error, "Invalid password"}
        else
          {:ok, user}
        end
    end
  end

  def create(username, password) do
    case get(username) do
      {:ok, _} ->
        {:error, "User already exists"}

      {:error, _} ->
        conn = Core.Redis.get_connection()
        salt = :crypto.strong_rand_bytes(10)
        password_hash = Core.Sha.encrypt(password, salt)

        {:ok, _} =
          Redix.command(conn, [
            "GRAPH.QUERY",
            "Users",
            Queries.create(username, password_hash, salt)
          ])

        {:ok, new_user} = get(username)

        {:ok, new_user}
    end
  end
end
