defmodule Core.Scan do
  alias Core.Categories.Category
  alias Core.Scan.Save

  defp cast(map) do
    %Category{
      name: Map.get(map, "name"),
      created_at: Map.get(map, "createdAt"),
      preview_image: Map.get(map, "previewImage")
    }
  end

  defp decode(%{body: body}) do
    Poison.decode!(body)
    |> cast()
  end

  defp purge_body(body) do
    String.split(body, "base64,")
    |> List.last()
  end

  def execute(current_user, body) do
    # result = HTTPoison.post!("#{Application.fetch_env!(:core, :predictor_url)}/predict", purge_body(body))
    # |> decode()

    result = %{
      name: "unknown",
      created_at: "",
      preview_image: ""
    }

    # {:ok} = Save.execute(current_user, "birra_moretti")

    result
  end
end
