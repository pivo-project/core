defmodule Core.Scan.Queries do
  import ExCypher
  
  def create(username, name, timestamp) do
    cypher do
      merge node(:user, [:User], %{username: username})
      merge node(:scan, [:Scan], %{name: name})
      merge (node(:user) -- rel([:SCANNED], %{timestamp: timestamp}) -> node(:scan))
    end
  end

  def get(username) do
    cypher do
      match (node(:user, [:User]) -- rel(:scanned, [:SCANNED]) -> node(:scan, [:Scan]))
      where user.username == username
      return :scanned, :scan
    end
  end
end
