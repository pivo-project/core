defmodule Core.Scan.Get do
  alias Core.Redis
  alias Core.Redis.Query
  alias Core.Scan.Queries

  defp format_scan(results) do
    categories = Core.Categories.get()

    Enum.map(results, fn item ->
      {%{"timestamp" => timestamp}, %{"name" => name}} = item
      category = Enum.find(categories, fn current -> current.name == name end)

      %{
        name: name,
        preview_image: category.preview_image,
        timestamp: "#{timestamp}"
      }
    end)
  end

  def execute(current_user) do
    conn = Redis.get_connection()

    {:ok, [["scanned", "scan"], result, _]} =
      Redix.command(conn, ["GRAPH.QUERY", "Users", Queries.get(current_user)])

    {:ok, Query.parse_result(result) |> format_scan()}
  end
end
