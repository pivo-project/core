defmodule Core.Scan.Save do
  alias Core.Scan.Queries

  def execute(username, name) do
    timestamp = :os.system_time(:millisecond)
    query = Queries.create(username, name, timestamp)

    conn = Core.Redis.get_connection()

    {:ok, _} = Redix.command(conn, ["GRAPH.QUERY", "Users", query])

    {:ok}
  end
end
