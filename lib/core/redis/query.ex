defmodule Core.Redis.Query do
  defp parse_item([[["id", id], _, ["properties", properties]]]) do
    Enum.reduce(properties, %{"id" => id}, fn [name, value], acc -> Map.put(acc, name, value) end)
  end

  defp parse_item([[["id", id], _, _, ["properties", properties]]]) do
    Enum.reduce(properties, %{"id" => id}, fn [name, value], acc -> Map.put(acc, name, value) end)
  end

  defp parse_item([[["id", id], _, _, _, ["properties", properties]]]) do
    Enum.reduce(properties, %{"id" => id}, fn [name, value], acc -> Map.put(acc, name, value) end)
  end

  defp parse_item(item) when length(item) == 2 do
    [first, second] = item

    {parse_item([first]), parse_item([second])}
  end

  def parse_result(result) do
    Enum.map(result, &parse_item/1)
  end
end
