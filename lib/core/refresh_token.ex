defmodule Core.RefreshToken do
  use Joken.Config

  def token_config do
    %{}
    |> Joken.Config.add_claim(
      "exp",
      fn -> Date.utc_today() |> Date.add(60) |> Date.to_iso8601() end,
      fn _ -> true end
    )
  end

  defp build_claims(user) do
    %{"username" => Map.get(user, "username")}
  end

  def gen_for_user(user) do
    signer = Joken.Signer.create("HS256", Map.get(user, "salt"))

    {:ok, token, _} = generate_and_sign(build_claims(user), signer)

    token
  end

  def verify_with_salt(token) do
    {:ok, %{"username" => username}} = Joken.peek_claims(token)
    {:ok, %{"salt" => salt}} = Core.Users.get(username)

    signer = Joken.Signer.create("HS256", salt)

    case Joken.verify(token, signer) do
      {:ok, _} -> {:ok}
      {:error, _} -> {:error}
    end
  end

  def verify_saved(token) do
    {:ok, %{"username" => username}} = Joken.peek_claims(token)

    case Core.Users.Token.exists?(username) do
      true -> {:ok}
      false -> {:error}
    end
  end

  def get_username(token) do
    case Joken.peek_claims(token) do
      {:ok, %{"username" => username}} -> {:ok, username}
      {:error, _} -> {:error}
    end
  end
end
