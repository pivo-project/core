defmodule Core.Categories.Category do
  defstruct [:name, :created_at, :preview_image]
end
