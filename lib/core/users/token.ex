defmodule Core.Users.Token do
  def ensure_object() do
    conn = Core.Redis.get_connection()

    {:ok, activeTokens} = Redix.command(conn, ["JSON.GET", "activeTokens"])

    if is_nil(activeTokens) do
      IO.puts("Creating active tokens object...")
      {:ok, "OK"} = Redix.command(conn, ["JSON.SET", "activeTokens", ".", "{}"])
    else
      IO.puts("Active tokens object already exists, nothing to do")
    end
  end

  def save(token, username) do
    conn = Core.Redis.get_connection()

    case Redix.command(conn, ["JSON.SET", "activeTokens", ".#{username}", "\"#{token}\""]) do
      {:ok, "OK"} -> {:ok}
      {:error, _} -> {:error}
    end
  end

  def exists?(username) do
    conn = Core.Redis.get_connection()

    case Redix.command(conn, ["JSON.GET", "activeTokens", ".#{username}"]) do
      {:ok, _} -> true
      {:error, _} -> false
    end
  end

  def delete(username) do
    conn = Core.Redis.get_connection()

    case Redix.command(conn, ["JSON.DEL", "activeTokens", ".#{username}"]) do
      {:ok, _} -> {:ok, true}
      {:error, _} -> {:error, false}
    end
  end
end
