defmodule Core.Users.Queries do
  import ExCypher

  def get(username) do
    cypher do
      match node(:user, [:User], %{username: username})
      return :user
    end
  end

  def create(username, password_hash, salt) do
    cypher do
      create node(:user, [:User], %{username: username, password_hash: password_hash, salt: salt})
    end
  end
end
