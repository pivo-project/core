defmodule Core.Admins.EnsureDefault do
  @default_admins ["miuta.mihai@gmail.com"]

  def execute() do
    current_admins = Core.Admins.get()

    if Enum.sort(current_admins) == Enum.sort(@default_admins) do
      IO.puts "Default admins already exist, nothing to do"
    else
      IO.puts "Creating default admins..."
      Core.Admins.set(@default_admins)
    end
  end
end
