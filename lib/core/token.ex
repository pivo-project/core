defmodule Core.Token do
  use Joken.Config

  def gen_for_user(user) do
    signer = Joken.Signer.create("HS256", Map.get(user, "salt"))
    {:ok, token, _} = generate_and_sign(%{"username" => Map.get(user, "username")}, signer)

    token
  end

  def get_username(token) do
    {:ok, %{"username" => username}} = Joken.peek_claims(token)

    username
  end

  def verify_for_user(token, user) do
    signer = Joken.Signer.create("HS256", Map.get(user, "salt"))

    case Joken.verify(token, signer) do
      {:ok, _} -> {:ok}
      {:error, _} -> {:error, "Invalid authorization token"}
    end
  end
end
