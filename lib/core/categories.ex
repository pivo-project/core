defmodule Core.Categories do
  alias Core.Categories.Category

  defp cast(map) do
    %Category {
      name: Map.get(map, "name"),
      created_at: Map.get(map, "createdAt"),
      preview_image: Map.get(map, "previewImage")
    }
  end

  defp decode(%{body: body}) do
    Poison.decode!(body)
    |> Enum.map(&cast/1)
  end

  def get() do
    HTTPoison.get!("#{Application.fetch_env!(:core, :predictor_url)}/categories")
    |> decode()
  end
end
