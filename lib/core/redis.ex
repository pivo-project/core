defmodule Core.Redis do
  def get_connection() do
    redis_host = Application.fetch_env!(:core, :redis_host)
    redis_port = Application.fetch_env!(:core, :redis_port)

    {:ok, conn} = Redix.start_link(host: redis_host, port: redis_port)

    conn
  end
end
