defmodule Core.Application do
  use Application

  @impl true
  def start(_type, _args) do
    children = [
      CoreWeb.Telemetry,
      {Phoenix.PubSub, name: Core.PubSub},
      CoreWeb.Endpoint
    ]

    Core.Admins.EnsureDefault.execute()
    Core.Users.Token.ensure_object()

    opts = [strategy: :one_for_one, name: Core.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    CoreWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
