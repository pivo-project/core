#!/bin/bash

docker container stop core
docker container stop predictor
docker container stop database
docker container stop tensorflow

docker container rm core
docker container rm predictor
docker container rm database
docker container rm tensorflow

docker image rm registry.gitlab.com/pivo-project/predictor:build
docker image rm registry.gitlab.com/pivo-project/tensorflow:gpu

docker compose up --build -d
